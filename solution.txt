1. List the books Authored by Marjorie Green.
	Ans. 
		=======================================
		| Title				
		=======================================
		| The Busy Executive's Database Guide 
		=======================================
		| You Can Combat Computer Stress      
		=======================================

2. List the books Authored by Michael O'Leary.
	Ans.
		=======================================
		| Title								  
		=======================================
		| Cooking with Computers 			  
		=======================================

3. Write the author/s of "The Busy Executives Database Guide".
	Ans.
		=======================================
		| Author							  
		=======================================
		| Marjorie Green 					  
		=======================================
		| Abraham Bennet 			     	  
		=======================================

4. Identify the publisher of "But Is It User Friendly?".
	Ans.
		=======================================
		| Publisher							  
		=======================================
		| Algodata Infosystems 				  
		=======================================

5. List the books published by Algodata Infosystems.
	Ans.
		=======================================
		| Title								  
		=======================================
		| The Busy Executive's Database Guide 
		=======================================
		| Cooking with Computers      		  
		=======================================
		| Straight Talk About Computers       
		=======================================
		| But Is It User Friendly?			  
		=======================================
		| Secrets of Silicon Valley           
		=======================================
		| Net Etiquette						  
		=======================================